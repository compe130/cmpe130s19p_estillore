def dijkstra(graph, src, dest, visited=[], distances={}, predecessors={}):
    """ calculates a shortest path tree routed in src
    """
    # a few sanity checks
    if src not in graph:
        raise TypeError('The root of the shortest path tree cannot be found')
    if dest not in graph:
        raise TypeError('The target of the shortest path cannot be found')
        # ending condition
    if src == dest:
        # We build the shortest path and display it
        path = []
        pred = dest
        while pred != None:
            path.append(pred)
            pred = predecessors.get(pred, None)
        # reverses the array, to display the path nicely
        readable = path[0]
        for index in range(1, len(path)): readable = path[index] + '--->' + readable
        # prints results
        # print('shortest path - array: ' + str(path))
        print("\npath: " + readable + ",   distance = " + str(distances[dest]) + " miles")
    else:
        if not visited:
            distances[src] = 0
        # visit the neighbors
        for neighbor in graph[src]:
            if neighbor not in visited:
                new_distance = distances[src] + graph[src][neighbor]
                if new_distance < distances.get(neighbor, float('inf')):
                    distances[neighbor] = new_distance
                    predecessors[neighbor] = src
        # mark as visited
        visited.append(src)
        # now that all neighbors have been visited: recurse
        # select the non visited node with lowest distance 'x'
        # run Dijskstra with src='x'
        unvisited = {}
        for k in graph:
            if k not in visited:
                unvisited[k] = distances.get(k, float('inf'))
        x = min(unvisited, key=unvisited.get)
        dijkstra(graph, x, dest, visited, distances, predecessors)


if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testName']
    # unittest.main()
    graph = {
             "Santa Rosa": {"Oakland": 59, "San Francisco": 55, "Sacracmento": 98},
             "Sacramento": {"Santa Rosa": 98, "Oakland": 82, "Stockton": 49},
             "Oakland": {"Santa Rosa": 59, "Sacramento": 82, "San Francisco": 13, "Fremont": 27, "Livermore": 33},
             "San Francisco": {"Santa Rosa": 55, "Oakland": 13, "Monterey": 113, "San Jose": 55, "Fremont": 38},
             "Stockton": {"Sacramento": 49, "Livermore": 40},
             "Livermore": {"Oakland": 33, "Stockton": 40, "Fremont": 19},
             "Fremont": {"San Francisco": 38, "San Jose": 18, "Oakland": 27, "Livermore": 19},
             "San Jose": {"San Francisco": 55, "Fremont": 18, "Monterey": 72, "Fresno": 150},
             "Monterey": {"San Francisco": 113, "San Jose": 72, "Santa Maria": 174},
             "Santa Maria": {"Monterey": 174, "Santa Barbara": 75, "Fresno": 170, "Bakersfield": 123},
             "Santa Barbara": {"Santa Maria": 75, "Los Angeles": 107},
             "Los Angeles": {"Santa Barbara": 107, "Bakersfield": 112, "San Diego": 128},
             "Fresno": {"San Jose": 150, "Santa Maria": 170, "Visalia": 43},
             "Visalia": {"Fresno": 43, "Bakersfield": 80},
             "Bakersfield": {"Visalia": 80, "Santa Maria": 123, "Los Angeles": 112},
             "San Diego": {"Los Angeles": 128}}

    print("\nThis program will find the shortest route from your location to your destination in California \n")
    start_a = input("Where will you departure from: ")
    destination = input("Where do you want to go: ")
    # dijkstra(graph, "San Francisco", "Bakersfield")
    dijkstra(graph, start_a, destination)